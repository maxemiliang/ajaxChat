<?php

	require "db.php";

	$db->query("CREATE TABLE chat (id int PRIMARY KEY, user CHAR(60), msg TEXT, read INT DEFAULT 0);");
	$db->query("CREATE TABLE unread (uid varchar(500) PRIMARY KEY, msgID int);");

	$_SESSION["uid"] = bin2hex(openssl_random_pseudo_bytes(6));


?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Chite-Chat</title>
	<link rel="stylesheet" href="style.css">
	<script src="jquery.js"></script>
	<script defer src="script.js"></script>
</head>
<body>
	<h1>Welcome to the chat room: <span class="welcome"></span></h1>
	<h4>Please insert your username below:</h4>
	<input id="username" name="username" maxlength="60" type="text" placeholder="Username:">
	<br>
	<br>
	<div class="chat">
		<div class="chatbox">
			<h4>Chat-box</h4>
			<ul class="messages">

			</ul>
		</div>
			<textarea class="form-text" id="msg" placeholder="Type something..."></textarea><button class="send">Submit</button>
	</div>
</body>
</html>