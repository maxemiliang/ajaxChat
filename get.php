<?php

require "db.php";
if (isset($_GET["all"])) {

    if ($_GET["all"] == "1") {

        $q = $db->query("SELECT user, msg FROM chat");

    } else {

        $q = $db->query("SELECT user, msg FROM chat WHERE read = 0");

        $q2 = $db->query("UPDATE chat SET read = ? WHERE read = 1");
    }

} else {

    $err = array("error" => "Wrong request!");

}

if (isset($q)) {

    echo json_encode($q->fetchAll(PDO::FETCH_ASSOC));

} else {

    echo json_encode($err);

}
